(setq neo4j-packages '(org
                       n4js
                       (ob-cypher :location (recipe :fetcher gitlab :repo "mroh69/ob-cypher"))))

(defun neo4j/init-n4js ()
  (use-package n4js
    :defer t))

(defun neo4j/pre-init-org ()
  (spacemacs|use-package-add-hook org
    :post-config (add-to-list 'org-babel-load-languages '(cypher . t))))

(defun neo4j/init-ob-cypher ()
  (use-package ob-cypher
    :defer t))
