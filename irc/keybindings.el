(spacemacs/set-leader-keys
  "aiz" 'znc-erc)

(spacemacs/set-leader-keys-for-major-mode 'erc-mode
  "s" 'erc-status-sidebar-toggle)
