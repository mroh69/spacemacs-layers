(setq irc-packages '(erc
                     znc
                     ercn
                     erc-status-sidebar))

(defun irc/init-znc ()
  (use-package znc
    :defer t))

(defun irc/init-ercn ()
  (use-package ercn   ;; autoload seems broken, so no defer
    ))

(defun irc/init-erc-status-sidebar ()
  (use-package erc-status-sidebar
    :defer t))

(defun irc/pre-init-erc ()
  (setq erc-enable-notifications nil))   ;; because of ercn

(defun irc/post-init-erc ()
  (setq erc-hide-list '("JOIN" "PART" "QUIT")
        erc-prompt-for-nickserv-password 'nil
        erc-max-buffer-size 60000 ;; Truncate buffers so they don't hog core.
        erc-notify-interval 300
        erc-autoaway-idle-seconds 1800)

  (add-to-list 'erc-modules 'notify) (add-to-list 'erc-modules 'autoaway)   ;; maybe better setq the whole list?
  (erc-truncate-mode t)
  (erc-update-modules)
  (with-eval-after-load 'erc-goodies   ;; erc makes imenu unusable
    (remove-hook 'erc-mode-hook 'erc-imenu-setup)))
